-- You can use this file to load seed data into the database using SQL statements

-- Workflow entries
insert into WorkflowEntry (id, collateral, title, status, reason, document, startDate, startUser, endDate, endUser, dueDate, lastUpdated, lastUpdatedBy, collated) values (0, 'VIN13469', 'IL - T0910-001', 130, 5, NULL, '2012-9-10 04:25:23', 'dxhezo', '2012-9-10 04:25:23', 'dxhezo', '2012-9-10 04:25:23', '2012-9-10 04:25:23', 'dxhezo', false)
insert into WorkflowEntry (id, collateral, title, status, reason, document, startDate, startUser, endDate, endUser, dueDate, lastUpdated, lastUpdatedBy, collated) values (1, 'VIN13470', 'IL - T0910-001', 130, 5, NULL, '2012-9-10 04:25:23', 'dxhezo', '2012-9-10 04:25:23', 'dxhezo', '2012-9-10 04:25:23', '2012-9-10 04:25:23', 'dxhezo', false)
insert into WorkflowEntry (id, collateral, title, status, reason, document, startDate, startUser, endDate, endUser, dueDate, lastUpdated, lastUpdatedBy, collated) values (2, 'VIN13471', 'IL - T0910-001', 130, 5, NULL, '2012-9-10 04:25:23', 'dxhezo', '2012-9-10 04:25:23', 'dxhezo', '2012-9-10 04:25:23', '2012-9-10 04:25:23', 'dxhezo', false)

-- Workflow notes
insert into WorkflowNote (id, workFlowId, noteType, noteTime, noteDescription) values (0, 1, 'phone', '2012-9-10 04:25:23', '215-858-8588')
insert into WorkflowNote (id, workFlowId, noteType, noteTime, noteDescription) values (1, 1, 'amount', '2012-9-10 04:25:23', '256.25')
insert into WorkflowNote (id, workFlowId, noteType, noteTime, noteDescription) values (2, 2, 'phone', '2012-9-10 04:25:23', '215-858-8588')
insert into WorkflowNote (id, workFlowId, noteType, noteTime, noteDescription) values (3, 2, 'amount', '2012-9-10 04:25:23', '25.12')
insert into WorkflowNote (id, workFlowId, noteType, noteTime, noteDescription) values (4, 2, 'text', '2012-9-10 04:25:23', 'Some description here')

--CREATE TABLE FILE_STORAGE (
--    FS_PK BIGINT AUTO_INCREMENT,
--    FILE_NAME VARCHAR(255) NOT NULL,
--    CONTENT LONGBLOB,
--    PRIMARY KEY(FS_PK)
--);
