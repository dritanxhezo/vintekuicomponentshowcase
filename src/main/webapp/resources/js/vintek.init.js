/* jQuery clear form
 * usage: $('selector').clearForm();
*/
$.fn.clearForm = function() {
	return this.each(function() {
		var type = this.type, tag = this.tagName.toLowerCase();
		if (tag == 'form'){
			return $(':input',this).clearForm();
		}
		if (type == 'text' || type == 'password' || tag == 'textarea'){
			this.value = '';
		}
		else if (type == 'checkbox' || type == 'radio'){
			this.checked = false;
		}
		else if (tag == 'select') {
			this.selectedIndex = -1;
		}
	}).trigger("liszt:updated");
};


// sliding panel
vtt_slidingPanel = {
	init: function() {
		$("#slide_open").click(function(){
			$("#slide_content").slideDown("fast", function() {
				if ($(".pdfObject").length > 0) {
					var padHeight = 6;
					var padWidth = 2;
					if ($.browser.msie && $.browser.version<9) {
						padHeight = 2;
						padWidth = 0;
					}
					$(document.body).append('<iframe id="pdf_overlay" src="about:blank"/>');
					$("#pdf_overlay").css({	'z-index': parseInt($('#slide_wrapper').css('z-index')) - 10,
						  					'position': 'absolute', 
						  					'margin':'0 auto',
						  					'border':'0px',
						  					'top':parseInt($("#slide_wrapper").css('top')),
						  					'right':parseInt($("#slide_wrapper").css('right')) + 20 + "px",
						  					'bottom':parseInt($("#slide_wrapper").css('bottom')),
						  					'left':parseInt($("#slide_wrapper").css('left'))});
					$("#pdf_overlay").height(parseInt($("#slide_content").outerHeight()) + padHeight + "px");
					$("#pdf_overlay").width(parseInt($("#slide_content").outerWidth()) + padWidth + "px");
					$("#pdf_overlay").attr('frameborder','0px');
				}
			});
		});	
		
		$("#slide_close").click(function(){
			$("#slide_content").slideUp("fast", function() {
				$("#pdf_overlay").remove();
			});
		});
		
		$("#slide_close img").bind('mouseenter mouseleave', function(){
			$(this).toggleClass('round_x16_a round_x16_b');
		});
	}
};

vtt_barMenu = {
	init: function() {
        $('.btn.menu').hover(
        function(){
            $(this).find('ul').fadeIn('fast');
         },
         function(){
             $(this).find('ul').fadeOut('fast');
         });			
	}
};

//footer scroll to top
vtt_scrolltoTop = {
   init: function(){
	  $(".toTop").click(function(){
		 $('html, body').animate({ scrollTop: 0 }, 'slow');
		 return false;
	  });
   }
};

//jQuery tools tooltips 
vtt_tips = {
	init: function() {
		//bottom
		var tipBconf = {    
			layout:	'<div><span class="tipB_arrow"></span></div>',
			position: 'bottom center',
			offset: [10,0],
			delay: 100,
			predelay: 100
		};	
		//top
		var tipTconf = {    
			layout:	'<div><span class="tipT_arrow"></span></div>',
			position: 'top center',
			offset: [-10,0],
			delay: 100,
			predelay: 100
		};
		//right
		var tipRconf = {    
			layout		: '<div><span class="tipR_arrow"></span></div>',
			position	: 'center right',
			offset		: [0,10],
			delay		: 100,
			predelay	: 100
		};
		//left
		var tipLconf = {    
			layout		: '<div><span class="tipL_arrow"></span></div>',
			position	: 'center left',
			offset		: [0,-10],
			delay		: 100,
			predelay	: 100
		};
		
		//bottom position: tooltip(tipBconf)
		$('.lgutipB').tooltip();
		//top position: tooltip(tipTconf)
		$('.lgutipT').tooltip();
		//left position: tooltip(tipLconf)
		$('.lgutipL').tooltip();
		//right position: tooltip(tipRconf)
		$('.lgutipR').tooltip();
	}
};

//sidebar accordion
sidebar = {
	accordion: function(){
		
		$('#sidebar').children('div.micro').each(function (e) {
            $(this).attr('rel', e);
		});
		var act_id = $('#sidebar li.active').closest('div.micro').attr('rel');
		
		$('#sidebar').microAccordion({
		   openSingle: true,
		   closeFunction: function (obj) {
			  obj.slideUp('fast');
		   },
		   toggleFunction: function (obj) {
				 obj.slideToggle('fast');
		   },
		   defaultOpen: act_id
		});
	}
};