package com.vintek.ui.servlet;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/*
 * Could also add in web.xml:
 * <multipart-config>
 *   <location>/tmp</location>
 *   <max-file-size>20848820</max-file-size>
 *   <max-request-size>418018841</max-request-size>
 *   <file-size-threshold>1048576</file-size-threshold>
 * </multipart-config>
 * (Same as config: location="/tmp", fileSizeThreshold=1024*1024, maxFileSize=1024*1024*5, maxRequestSize=1024*1024*5*5
 */
public abstract class AbstractUploadServlet extends HttpServlet {

	public AbstractUploadServlet() {
		super();
	}

	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			String successFiles = fileUploadWithDesiredFilePathAndName(request);
			response.getWriter().println("Uploaded Succesfully!|File(s) " + successFiles + " were uploaded succesfully!");
		} catch (Exception e) {
			response.getWriter().println("Error Uploading!|" + e.getMessage());
		}
	}

	public String fileUploadWithDesiredFilePathAndName(HttpServletRequest request) throws IOException, ServletException {
		InputStream inputStream = null;
		String successFiles = "";
		try {
			for (Part part : request.getParts()) {
				if (!"file".equalsIgnoreCase(part.getName())) continue;
				System.out.println(part.getName());
				System.out.println("part.getHeader(\"content-disposition\") = " + part.getHeader("content-disposition"));

				inputStream = request.getPart(part.getName()).getInputStream();
				int i = inputStream.available();
				byte[] b = new byte[i];
				inputStream.read(b);
				System.out.println("Length : " + b.length);

				String fileName = extractFileName(part);

				if (persistFile(request, b, fileName)) {
					successFiles = successFiles + ", " + fileName;
				}
				inputStream.close();
				part.delete();  // Cleanup temporary storage.
			}
		} catch (Exception e) {
			System.out.println("Unable to Upload File: " + e);
			e.printStackTrace();
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return successFiles;
	}

	private String extractFileName(Part part) {
		String fileName = "";
		for (String temp : part.getHeader("content-disposition").split(";")) {
			if (temp.trim().startsWith("filename")) {
				fileName = temp.substring(temp.indexOf('=') + 1).trim().replace("\"", "");
				int fileStarts = fileName.lastIndexOf(File.separatorChar); // if the browser (IE) sent in the path as well as the filename, parse that out
				if (fileStarts > 0) {
					fileName = fileName.substring(fileStarts + 1);
				}
			}
		}
		return fileName;
	}

	abstract boolean persistFile(HttpServletRequest request, byte[] b, String fileName);
}