package com.vintek.ui.servlet;

import javax.ejb.EJB;
import com.vintek.persistence.model.FileStorageEntity;
import com.vintek.persistence.data.IFileStorageSessionFacadeLocal;

import javax.servlet.annotation.WebServlet;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;

@WebServlet(description = "FileDBUploadServlet Description", urlPatterns = { "/FileDBUploadServlet" })
@MultipartConfig(location = "c:/tmp/FileUpload_Servlet")
public class FileDBUploadServlet extends AbstractUploadServlet {
	private static final long serialVersionUID = 8004070222956311557L;
	
	@EJB
    IFileStorageSessionFacadeLocal fsSessionFacade;

	boolean persistFile(HttpServletRequest request, byte[] b, String fileName) {
		try {			
			FileStorageEntity fsEntity = new FileStorageEntity(fileName, b);
			fsEntity.setDescription(request.getParameter("description"));
			fsEntity.setMapFile(request.getParameter("map"));
			fsEntity.setHeaderRecord(request.getParameter("headerRecord").equalsIgnoreCase("true"));
			fsEntity.setSendEmail(request.getParameter("sendEmail").equalsIgnoreCase("true"));
			fsEntity.setEmail(request.getParameter("email"));
			
			fsSessionFacade.create(fsEntity);
			System.out.println("files is db:" + fsSessionFacade.count());
			return true;
		} catch (Exception e) {			
			return false;
		}
	}
}
