package com.vintek.ui.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(description = "Heartbeat servlet to keep the user session alive", urlPatterns = { "/poll" })
public class HeartbeatServlet extends HttpServlet {
	private static final long serialVersionUID = -9178949553929140174L;

	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("heartbeat servlet called. keeping session alive ...");
		request.getSession();
		response.getWriter().println("OK");
	}
}
