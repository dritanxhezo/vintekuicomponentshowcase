package com.vintek.ui.servlet;

import java.io.FileOutputStream;

import javax.servlet.annotation.WebServlet;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;

@WebServlet(description = "FileUploadServlet Description", urlPatterns = { "/FileUploadServlet" })
@MultipartConfig(location = "c:/tmp/FileUpload_Servlet")
public class FileUploadServlet extends AbstractUploadServlet {
	private static final long serialVersionUID = -8269557562737566160L;

	boolean persistFile(HttpServletRequest request, byte[] b, String fileName) {
		FileOutputStream outputStream = null;
		boolean retVal = false;
		try {			
			String uploadDir = System.getProperty("jboss.server.base.dir") + "/upload";
			System.out.println("File will be Uploaded at: " + uploadDir + "/" + fileName);				
			outputStream = new FileOutputStream(uploadDir + "/" + fileName);
			outputStream.write(b);
			retVal = true;
		} catch (Exception e) {			
			retVal = false;
		} finally {
			if (outputStream != null) {
				try {
					outputStream.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return retVal;
	}	
}
