package com.vintek.ui.controller;

import java.io.Serializable;

import javax.enterprise.inject.Produces;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.vintek.ui.component.VCheckTreeNode;
import com.vintek.ui.model.User;

@ManagedBean
@SessionScoped
public class UsersController implements Serializable {
	private static final long serialVersionUID = 8315080712997657918L;
	
	@Inject
	private User selectedUser;
	
	public User getSelectedUser() {
		return selectedUser;
	}

	public void saveUser() throws Exception {
		System.out.println("user saved: " + selectedUser);
	}

	public void assignOrg() throws Exception {
	    for (String selectedItem : selectedUser.getSelectedAvailableOrgs()) {
	        System.out.println("Selected item: " + selectedItem);
	    }
		
	}
	
	public void printTestCheckbox() throws Exception {
		System.out.println("user saved: " + selectedUser);
		for (VCheckTreeNode node: selectedUser.getTestChekboxes()) {
			System.out.println(node.getName() + ": " + node.getValue());
		}
	}
	

}
