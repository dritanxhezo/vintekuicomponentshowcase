package com.vintek.ui.controller;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Model;
import javax.enterprise.inject.Produces;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import com.vintek.midtier.businessdelegate.VintekTimeBD;
import com.vintek.persistence.model.WorkflowEntry;
import com.vintek.persistence.model.WorkflowNote;

import java.io.Serializable;

//The @Model stereotype is a convenience mechanism to make this a request-scoped bean that has an EL name
//Read more about the @Model stereotype in this FAQ:
//http://sfwk.org/Documentation/WhatIsThePurposeOfTheModelAnnotation
//@Model
//@Named
//@SessionScoped
@ManagedBean
//@javax.faces.bean.SessionScoped
@ViewScoped
public class WorkFlowController implements Serializable {

	private static final long serialVersionUID = 2843402763500317295L;

	@Inject
	private FacesContext facesContext;

	@Inject
	private VintekTimeBD vttDelegate;

	private WorkflowEntry newWorkFlowEntry;
	private List<WorkflowEntry> workFlowEntries;

	private WorkflowNote newWorkFlowNote;
	
	//@Produces
	//@Named
	public List<WorkflowEntry> getWorkFlowEntries() {
		return workFlowEntries;
	}

	//@Produces
	//@Named
	public WorkflowEntry getNewWorkFlowEntry() {
		return newWorkFlowEntry;
	}

	public WorkflowNote getNewWorkFlowNote() {
		return newWorkFlowNote;
	}
	
	public void addWorkFlow() throws Exception {
		vttDelegate.addWorkflowEntry(newWorkFlowEntry);
		newWorkFlowEntry = new WorkflowEntry();
		workFlowEntries = vttDelegate.getWorkFlowEntries("1234");
		facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "New Workflow Entry added!", "New Workflow Entry added succesfully"));
	}

	public void addWorkFlowNote() throws Exception {
		vttDelegate.addWorkflowNote(newWorkFlowNote);
		newWorkFlowNote = new WorkflowNote();
		workFlowEntries = vttDelegate.getWorkFlowEntries("1234");
		facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "New Workflow Entry Note added!", "New Workflow Entry Note added succesfully"));
	}
	
	@PostConstruct
	public void initWorkflowEntries() {
		newWorkFlowEntry = new WorkflowEntry();
		newWorkFlowNote = new WorkflowNote();
		workFlowEntries = vttDelegate.getWorkFlowEntries("1234");
//		for (WorkflowEntry wkEntry: workFlowEntries) {
//			System.out.println(wkEntry.getCollateral());
//			for (WorkflowNote note: wkEntry.getWorkFlowNotes()) {
//				System.out.println(note.getNoteDescription());
//			}			
//		}
	}


}
