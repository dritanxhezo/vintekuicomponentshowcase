package com.vintek.ui.controller;

import com.vintek.ui.model.UploadFile;
import com.vintek.ui.util.FacesUtil;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.Stateless;

@Stateless
public class PersonBean {

    /**
     * Saves a vcard for the person with supplied username
     */
    public void saveVCard(UploadFile file, String username){
        // Make use of session for example. No need to complicate with connection to filesystem or database
        System.out.println("Saving vcard for " + username);
        getVcards().put(username, file);
    }


    public UploadFile findVCard(String username){
        System.out.println("Getting vcard for" + username);
        return (UploadFile) getVcards().get(username);
    }

    public void removeVCard(String username){
        System.out.println("Removing vcard for" + username);
        getVcards().remove(username);
    }

    private Map getVcards(){
        Map vcards = (Map)FacesUtil.getRequest().getSession().getAttribute("vcards");
        if(vcards == null){
            vcards = new HashMap();
            FacesUtil.getRequest().getSession().setAttribute("vcards", vcards);
        }
        return vcards;
    }
 
}