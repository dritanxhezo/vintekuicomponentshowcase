package com.vintek.ui.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
//import theironicprogrammer.session.PersonUpdaterLocal;
import com.vintek.ui.http.MultiPart;
import com.vintek.ui.http.MultipartRequestWrapper;
import com.vintek.ui.model.UploadFile;
import com.vintek.ui.util.FacesUtil;


@ManagedBean(name="vcardBean")
@RequestScoped
public class VCardBean implements Serializable{
    @EJB PersonBean personBean;

    public String uploadVCard(){
        System.out.println("uploadVCard...");
        HttpServletRequest req = FacesUtil.getRequest();
        if(req instanceof MultipartRequestWrapper){
            MultipartRequestWrapper multi = (MultipartRequestWrapper)req;

            UploadFile uf = multi.findFile("vcardFile");
            if(uf != null){
                String fileExt = uf.getFileName().substring(uf.getFileName().lastIndexOf("."));
                System.out.println("Extension" + fileExt);
                // should actually check the extension, but no big deal for example
                personBean.saveVCard(uf, "testuser");

                FacesUtil.getFlash().put("messageCode", "vcard.upload.ok");
                FacesUtil.getFlash().put("fileName", uf.getFileName());
            }

        }
        return "vCardForm";
    }

    public String findVCard(){
        UploadFile uf = personBean.findVCard("testuser");
        if(uf == null) {
            FacesUtil.getFlash().put("messageCode", "vcard.upload.err.load");
            FacesUtil.getFlash().put("messageType", "errors");

        } else {
            ServletOutputStream output = null;
            try {
                output = FacesUtil.getResponse().getOutputStream();
                FacesUtil.getResponse().setHeader("Content-disposition", "attachment; filename=" + uf.getFileName());
                FacesUtil.getResponse().setContentType(uf.getContentType());
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                baos.write(uf.getFileData());
                FacesUtil.getResponse().setCharacterEncoding("UTF-8");
                FacesUtil.getResponse().setContentLength(baos.size());
                baos.writeTo(output);
                output.flush();
            } catch (IOException ex) {
                FacesUtil.getFlash().put("messageCode", "vcard.upload.err.load");
                FacesUtil.getFlash().put("messageType", "errors");
                Logger.getLogger(VCardBean.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    output.close();
                } catch (IOException ex) {
                    FacesUtil.getFlash().put("messageCode", "vcard.upload.err.load");
                    FacesUtil.getFlash().put("messageType", "errors");
                    Logger.getLogger(VCardBean.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return "vCardForm";
    }

    public String removeVCard(){
        personBean.removeVCard("testuser");
        FacesUtil.getFlash().put("messageCode", "vcard.remove.ok");
        FacesUtil.getFlash().put("messageType", "messages");
        return "vCardForm";
    }

}