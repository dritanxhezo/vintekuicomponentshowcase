package com.vintek.ui.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.vintek.ui.component.VCheckTreeNode;

public class User implements Serializable {
	private static final long serialVersionUID = 5007752685563938314L;
	
    private List<String> selectedAvailableOrgs = new ArrayList<String>();
    private List<SelectItem> availableOrgs;
	
	private VCheckTreeNode userPermissions;
	private List<VCheckTreeNode> testChekboxes = new ArrayList<VCheckTreeNode>();
	
	@PostConstruct
	public void init() {	   		
		userPermissions = new VCheckTreeNode("All Permissions",
							 new VCheckTreeNode("Standard Permissions",
					                new VCheckTreeNode("View", false),
					                new VCheckTreeNode("Reports", true),
					                new VCheckTreeNode("View Attachment", false),
					                new VCheckTreeNode("Create Notes", false),
					                new VCheckTreeNode("Add", false),
					                new VCheckTreeNode("Update", false),
					                new VCheckTreeNode("Delete", false),
					                new VCheckTreeNode("Delete Attachment", false),
					                new VCheckTreeNode("Change Title Organization", false),
					                new VCheckTreeNode("Edit Acct/Org ID", false),
					                new VCheckTreeNode("Update Loan Collateral Only", false)),
					          new VCheckTreeNode("Advanced Permissions",
			                        new VCheckTreeNode("Issue Transactions", false),
			                        new VCheckTreeNode("Process Imports", false),
			                        new VCheckTreeNode("Destroy Paper Title", false),
			                        new VCheckTreeNode("DMV Documents", false),
			                        new VCheckTreeNode("Workflow", false),
			                        new VCheckTreeNode("Workflow (Read Only)", false),
			                        new VCheckTreeNode("Hide Phone icon on Workflow", false),
			                        new VCheckTreeNode("Hide Complete button on Workflow", false)),
			                  new VCheckTreeNode("Special Permissions",
			                        new VCheckTreeNode("Block Add Paper Title", false),
			                        new VCheckTreeNode("Block Settings", false),
			                        new VCheckTreeNode("Maintenance Release Only", false),
			                        new VCheckTreeNode("Delete Active Records", false))
		                 );
		
		getTestChekboxes().add(new VCheckTreeNode("View", false));
		getTestChekboxes().add(new VCheckTreeNode("Reports", true));
		getTestChekboxes().add(new VCheckTreeNode("View Attachment", false));
		getTestChekboxes().add(new VCheckTreeNode("Create Notes", false));
		getTestChekboxes().add(new VCheckTreeNode("Add", false));
		getTestChekboxes().add(new VCheckTreeNode("Update", false));
		getTestChekboxes().add(new VCheckTreeNode("Delete", false));
		getTestChekboxes().add(new VCheckTreeNode("Delete Attachment", false));
		getTestChekboxes().add(new VCheckTreeNode("Change Title Organization", false));
		getTestChekboxes().add(new VCheckTreeNode("Edit Acct/Org ID", false));
		getTestChekboxes().add(new VCheckTreeNode("Update Loan Collateral Only", false));
		
		availableOrgs = new ArrayList<SelectItem>();
		availableOrgs.add(new SelectItem("1890", "1890 - VINtek Test"));
		availableOrgs.add(new SelectItem("2643", "2643 - 1st Summit Bank"));
		availableOrgs.add(new SelectItem("3009", "3009 - 25th Street Auto Sales"));
		availableOrgs.add(new SelectItem("2819", "2819 - 30 West Auto Sales"));
	}
	
 
    public VCheckTreeNode getUserPermissions() {
        return userPermissions;
    }

	public List<VCheckTreeNode> getTestChekboxes() {
		return testChekboxes;
	}
    
	public List<String> getSelectedAvailableOrgs() {
		return selectedAvailableOrgs;
	}

	public void setSelectedAvailableOrgs(List<String> selectedAvailableOrgs) {
		this.selectedAvailableOrgs = selectedAvailableOrgs;
	}

	public List<SelectItem> getAvailableOrgs() {
		return availableOrgs;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
