package com.vintek.ui.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.vintek.ui.component.Field;

@ManagedBean
@ViewScoped
public class DynamicFields implements Serializable {
	private static final long serialVersionUID = 3631393146795408111L;
	
	private List<Field> fields;

    public DynamicFields() {
        fields = new ArrayList<Field>();
        fields.add(new Field("Name"));
        fields.add(new Field("Age"));
        fields.add(new Field("Address"));
    }

    public List<Field> getFields() {
        return fields;
    }

}