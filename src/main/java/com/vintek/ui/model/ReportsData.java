package com.vintek.ui.model;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.vintek.ui.component.VTreeNode;
import java.io.Serializable;

@ManagedBean
@SessionScoped
public class ReportsData implements Serializable {
	private static final long serialVersionUID = -8187843738374018219L;

	private VTreeNode reportsTree;
	
	@PostConstruct
	public void init() {
		int cnt = 1;
		reportsTree = new VTreeNode("Reports",
						new VTreeNode("Last Used", VTreeNode.BOOKMARK_FOLDER_TYPE, "", 
							new VTreeNode("Report 1", VTreeNode.BINARY_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Report 2", VTreeNode.ACCESS_FILE_TYPE, Integer.toString(cnt++))),
						new VTreeNode("Organization Reports", VTreeNode.MP3_FOLDER_TYPE, "",
							new VTreeNode("3153 - Automated Archive Report", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("3153 - Automated Release Report", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("2056 - VIN Match Report", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("2057 - Lien Holder Name Discrepancies Matched", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("2417 - Match Errors (Active Accounts Only)", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("3228 - Paper Title Lien Release Report", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("2114 - Accounts With VIN Errors (MNT)", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("2114 - MNT Failed Lien Filings", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("2114 - Monthly Release Report (Norma's Numbers)", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("2651 - True Perfected Liens", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("3078 - Average State Processing Time", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("3078 - Delayed Release Missing Address", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("3078 - Delayed Release Missing Loan", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("3078 - Import Errors", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("3078 - Import Log", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("3078 - Lien Holder Name Discrepancies", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("3078 - Match Error with Workflow", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("2227 - Legal or Charge Off Status", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("2227 - Paid Out ELT", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("4499 - Import Errors USAA", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("4499 - Match Errors USAA", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("1811 - Average State Processing Time", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("1811 - CITI RELEASE REPORT", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("1811 - CITI RELEASE REPORT - MANIFEST", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("1811 - CST_2651_ReassignmentReport", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("1813 - ReportTest", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("2355 - Unrecognized Titles", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++))),
						new VTreeNode("Reports", VTreeNode.MP3_FOLDER_TYPE, "",
							new VTreeNode("Title Control Report", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Accounts Pending Title", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Accounts With VIN Errors (All)", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Accounts With VIN Errors-Check Digit Only", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Active Accounts", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Auto Maintenance Release", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Billable Activity", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Billing Titles Received", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Changed Title Information", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Delayed Release Missing Address", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Delayed Release Missing Loan", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Delayed Release Missing Title", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Delayed Release Paper No Image ID", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Delayed Release Report", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Deleted Titles", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("DMV Errors", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("DMV Warnings", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Duplicate VIN Loan", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Duplicate VIN Title", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Export for Missing Title Reminders", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Failed Lien Filings", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Import Errors", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Import Log", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Incomplete Loan Address", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Letter Sent", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Lien Holder Name Discrepancies", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Lien Holder Name Discrepancies Matched", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Liens Released", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Loans Added", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Loans Started and Perfected", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Match Errors", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Match Errors ELT", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Match Errors Paper", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Non ELT Liens", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Perfected Liens", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Print Pending", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("PV-Report1", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Records Changed/Updated Via Import", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Rejected Titles", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Rejected Transactions", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Releases Pending", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Status Code Changes - Loan", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Status Code Changes - Title", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Tickler Log - Loan", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Tickler Log - Title", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Title Image ID", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Titles by Lien Expiration Date", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Titles In File Pre-Conversion", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Titles Printed", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Titles Received", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Titles Released", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Transaction History", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Transactions Pending", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Transactions Pending State Response", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Unrecognized ELT Titles", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Unrecognized Paper Titles", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Unrecognized Titles", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("VA Held Titles", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("Workflow Activity", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++))),
						new VTreeNode("Custom Reports", VTreeNode.MP3_FOLDER_TYPE, "",
							new VTreeNode("Custom Report 1", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++))),
						new VTreeNode("Admin Reports", VTreeNode.MP3_FOLDER_TYPE, "",
							new VTreeNode("User List", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)),
							new VTreeNode("User list by Group", VTreeNode.TXT_FILE_TYPE, Integer.toString(cnt++)))							
					);
	}
	
 
    public VTreeNode getTree() {
        return reportsTree;
    }
}
