package com.vintek.ui.model;

import java.io.Serializable;

import com.vintek.ui.component.IMessage;

public class Message implements IMessage, Serializable {
	private static final long serialVersionUID = -2713951934908238970L;

	private String messageType;
	private String messageString;
	private String messageDate;

	public Message(String messageType, String messageString, String messageDate) {
		this.messageType = messageType;
		this.messageString = messageString;
		this.messageDate = messageDate;
	}
	
	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	
	public String getMessageString() {
		return messageString;
	}

	public void setMessageString(String messageString) {
		this.messageString = messageString;
	}

	public String getMessageDate() {
		return messageDate;
	}

	public void setMessageDate(String messageDate) {
		this.messageDate = messageDate;
	}
}
