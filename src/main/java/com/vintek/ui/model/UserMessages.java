package com.vintek.ui.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;


@ManagedBean
@SessionScoped
public class UserMessages implements Serializable {

	private static final long serialVersionUID = 3974272627858978408L;
	private List<Message> messages;
	private List<String> tasks;
	
	@PostConstruct
	public void init() {
		messages = new ArrayList<Message>();
		messages.add(new Message("Releases", "3.20 Release Notes", "1/2/2013"));
		messages.add(new Message("Announcement", "SCUSA Announcement", "1/2/2013"));
		messages.add(new Message("Releases", "Scheduled Outage", "1/2/2013"));
		messages.add(new Message("Releases", "3.20 Release Notes", "1/2/2013"));
		messages.add(new Message("Announcement", "SCUSA Announcement", "1/2/2013"));
		messages.add(new Message("Releases", "Scheduled Outage", "1/2/2013"));
		messages.add(new Message("Releases", "3.20 Release Notes", "1/2/2013"));
		messages.add(new Message("Announcement", "SCUSA Announcement", "1/2/2013"));
		messages.add(new Message("Releases", "Scheduled Outage", "1/2/2013"));
		messages.add(new Message("Releases", "3.20 Release Notes", "1/2/2013"));
		messages.add(new Message("Announcement", "SCUSA Announcement", "1/2/2013"));
		messages.add(new Message("Releases", "Scheduled Outage", "1/2/2013"));
		
		tasks = new ArrayList<String>();
		tasks.add("task1");
	}

	public List<Message> getMessages() {
		return messages;
	}

	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}

	public List<String> getTasks() {
		return tasks;
	}

	public void setTasks(List<String> tasks) {
		this.tasks = tasks;
	}
	

}
