/**
 * 
 */
package com.vintek.ui.util;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.faces.context.FacesContext;

/**
 * @author dxhezo
 */
public class Resources {

	   @Produces
	   @RequestScoped
	   public FacesContext produceFacesContext() {
	      return FacesContext.getCurrentInstance();
	   }
}
