package com.vintek.ui.util;

import javax.faces.context.FacesContext;
import javax.faces.context.Flash;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class FacesUtil {

	public static HttpServletRequest getRequest() {
		HttpServletRequest req = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();		
		return req;
	}

	public static HttpServletResponse getResponse() {
		HttpServletResponse res = (HttpServletResponse)FacesContext.getCurrentInstance().getExternalContext().getResponse();		
		return res;
	}

	public static Flash getFlash() {
		Flash flash = FacesContext.getCurrentInstance().getExternalContext().getFlash();		
		return flash;
	}
}
