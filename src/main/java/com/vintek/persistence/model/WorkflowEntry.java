package com.vintek.persistence.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.Access;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.persistence.AccessType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@XmlRootElement
@Table(uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@Access(AccessType.FIELD)
@NamedQueries({
	@NamedQuery(name=WorkflowEntry.ALL,query="select w from WorkflowEntry w order by w.id"),
	@NamedQuery(name=WorkflowEntry.ALL_ORDER_COLL, query="select w from WorkflowEntry w order by w.collateral"),
	@NamedQuery(name=WorkflowEntry.BY_ID,query="Select w From WorkflowEntry w where w.id = :id"),	
})
public class WorkflowEntry implements Serializable {
	private static final long serialVersionUID = 4391862132181146362L;

	public final static String ALL = "com.vintek.persistence.model.WorkflowEntry.ALL";
	public static final String ALL_ORDER_COLL = "com.vintek.persistence.model.WorkflowEntry.ALL_ORDER_COLL";
    public final static String BY_ID = "com.vintek.persistence.model.WorkflowEntry.BY_ID";
    
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@NotEmpty
	private String collateral;

	@NotNull
	@NotEmpty
	private String title;

	@NotNull	
	private int status;
	
	private String reason;
	
	private String document;

	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	private Date startDate = Calendar.getInstance().getTime();

	@NotNull
	@NotEmpty
	private String startUser = "VTT";

	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	private Date endDate = Calendar.getInstance().getTime();

	@NotNull
	@NotEmpty
	private String endUser = "VTT";

	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	private Date dueDate =  Calendar.getInstance().getTime();

	private Boolean collated = false;	
	
	//@Basic(optional = false)
	//@Column(name = "LastUpdated", insertable = false, updatable = false)
	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastUpdated =  Calendar.getInstance().getTime();
	
	//@Basic(optional = false)
	//@Column(name = "LastUpdatedBy")
	@NotNull @NotEmpty
	private String lastUpdatedBy = "VTT";
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, targetEntity = WorkflowNote.class)
    //@JoinTable(name = "WorkflowNote", joinColumns = { @JoinColumn(name="workFlowId", referencedColumnName="id") })
	@JoinColumn(name="workFlowId")
	//@OneToMany(mappedBy="workFlowNote.workFlowId")
    private List<WorkflowNote> workFlowNotes;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCollateral() {
		return collateral;
	}

	public void setCollateral(String collateral) {
		this.collateral = collateral;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getDocument() {
		return document;
	}

	public void setDocument(String document) {
		this.document = document;
	}
	
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public String getStartUser() {
		return startUser;
	}

	public void setStartUser(String startUser) {
		this.startUser = startUser;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getEndUser() {
		return endUser;
	}

	public void setEndUser(String endUser) {
		this.endUser = endUser;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public Boolean getCollated() {
		return collated;
	}

	public void setCollated(Boolean collated) {
		this.collated = collated;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public List<WorkflowNote> getWorkFlowNotes() {
		return workFlowNotes;
	}

	public void setWorkFlowNotes(List<WorkflowNote> workFlowNotes) {
		this.workFlowNotes = workFlowNotes;
	}
}
