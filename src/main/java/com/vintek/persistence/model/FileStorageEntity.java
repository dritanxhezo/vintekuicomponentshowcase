package com.vintek.persistence.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "FILE_STORAGE")
@NamedQueries({
    @NamedQuery( name = "FileStorage.findAll", query = "SELECT f FROM FileStorageEntity f" ),
    @NamedQuery( name = "FileStorage.findByFsPk", query = "SELECT f FROM FileStorageEntity f WHERE f.fsPk = :fsPk" ),
    @NamedQuery( name = "FileStorage.findByFileName", query = "SELECT f FROM FileStorageEntity f WHERE f.fileName = :fileName" )
})
public class FileStorageEntity implements Serializable {
    private static final long serialVersionUID = -4796720242338042828L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "FS_PK")
    private Long fsPk;
    
    @Column(name = "FILE_NAME")
    private String fileName;
    
    @Lob
    @Column(name = "CONTENT")

    private byte[] content;

    private String description;
    
	private String mapFile;
	
	private boolean headerRecord;
	
	private boolean sendEmail;
	
	private String email;
    
    public FileStorageEntity() {

    }

    public FileStorageEntity(String fileName, byte[] content) {
        this.fileName = fileName;
        this.content = content;
    }

    public Long getFsPk() {
        return fsPk;
    }

    public void setFsPk(Long fsPk) {
        this.fsPk = fsPk;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    
    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }
    
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
    
	public String getMapFile() {
		return this.mapFile;
	}
	
	public void setMapFile(String mapFile) {
		this.mapFile = mapFile;
	}
	
	public boolean isHeaderRecord() {
		return headerRecord;
	}

	public void setHeaderRecord(boolean headerRecord) {
		this.headerRecord = headerRecord;
	}

	public boolean isSendEmail() {
		return sendEmail;
	}

	public void setSendEmail(boolean sendEmail) {
		this.sendEmail = sendEmail;
	}

	public String isEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}	
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fsPk != null ? fsPk.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if ( !(object instanceof FileStorageEntity) ) {
            return false;
        }

        FileStorageEntity other = ( FileStorageEntity ) object;
        if ( (this.fsPk == null && other.fsPk != null) || (this.fsPk != null && !this.fsPk.equals( other.fsPk )) ) {
            return false;
        }

        return true;
    }
    
    @Override
    public String toString() {
        return "FileStorage[ fsPk=" + fsPk + " ]";
    }
}