package com.vintek.persistence.data;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.vintek.persistence.model.WorkflowEntry;
import com.vintek.persistence.model.WorkflowNote;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class WorkFlowListProducer implements Serializable {
	private static final long serialVersionUID = 1877909208573383167L;
	
//	@Inject
//	private EntityManager em;
	
    @EJB
    private CrudService crudService;

    public List<WorkflowEntry> retrieveAllWorkFlowEntriesOrderedByCollateral(String loan) {
    	return crudService.findWithNamedQuery(WorkflowEntry.ALL_ORDER_COLL);    	
//		CriteriaBuilder cb = em.getCriteriaBuilder();
//		CriteriaQuery<WorkflowEntry> criteria = cb.createQuery(WorkflowEntry.class);
//		Root<WorkflowEntry> workFlowEntry = criteria.from(WorkflowEntry.class);
//		// Swap criteria statements if you would like to try out type-safe criteria queries, a new feature in JPA 2.0
//		// criteria.select(workFlowEntry).orderBy(cb.asc(workFlowEntry.get(WorkflowEntry_.collateral)));
//		criteria.select(workFlowEntry).orderBy(cb.asc(workFlowEntry.get("collateral")));
//		return em.createQuery(criteria).getResultList();    	
    }
//	public List<WorkflowEntry> retrieveAllWorkFlowEntriesOrderedByCollateral(String loan) {
//	}

	public WorkflowEntry addWorkflowEntry(WorkflowEntry wFlowEntry) {
		//em.persist(wFlowEntry);
		return crudService.create(wFlowEntry);
	}

	public WorkflowNote addWorkflowNote(WorkflowNote newWorkFlowNote) {
		//em.persist(newWorkFlowNote);
		return crudService.create(newWorkFlowNote);
	}
}
