package com.vintek.persistence.data;

import com.vintek.persistence.data.IFileStorageSessionFacadeLocal;
import com.vintek.persistence.model.FileStorageEntity;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class FileStorageSessionFacade extends AbstractSessionFacade<FileStorageEntity> implements IFileStorageSessionFacadeLocal {
	
    //Change the persistence context accordingly...
    @PersistenceContext(unitName = "primary")
    private EntityManager em;

    protected EntityManager getEntityManager() {
        return em;
    }

    public FileStorageSessionFacade() {
        super(FileStorageEntity.class);
    }
}


