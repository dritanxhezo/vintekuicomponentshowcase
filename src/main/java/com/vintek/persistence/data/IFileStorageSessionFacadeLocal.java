package com.vintek.persistence.data;

import com.vintek.persistence.model.FileStorageEntity;
import java.util.List;
import javax.ejb.Local;
@Local
public interface IFileStorageSessionFacadeLocal {
    public void create( FileStorageEntity fileStorageEntity );
    public void edit( FileStorageEntity fileStorageEntity );
    public void remove( FileStorageEntity fileStorageEntity );
    public FileStorageEntity find( Object id );
    public List<FileStorageEntity> findAll();
    public List<FileStorageEntity> findRange( int[] range );
    public int count();
}


