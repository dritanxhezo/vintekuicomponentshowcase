package com.vintek.midtier.businessdelegate;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
//import javax.persistence.EntityManager;

import com.vintek.persistence.data.CrudService;
import com.vintek.persistence.data.WorkFlowListProducer;
import com.vintek.persistence.model.WorkflowEntry;
import com.vintek.persistence.model.WorkflowNote;

//The @Stateless annotation eliminates the need for manual transaction demarcation
@Stateless
public class VintekTimeBD implements Serializable {
	private static final long serialVersionUID = 4197903735435736352L;

	@Inject
	private Logger log;
    
	@Inject
	private Event<WorkflowEntry> addWorkFlowEntryEvent;

	@Inject
	private WorkFlowListProducer workFlowListHelper;
	
	public void addWorkflowEntry(WorkflowEntry wFlowEntry) throws Exception {		
		workFlowListHelper.addWorkflowEntry(wFlowEntry);
		addWorkFlowEntryEvent.fire(wFlowEntry);
	}

	public List<WorkflowEntry> getWorkFlowEntries(String loan) {
		return workFlowListHelper.retrieveAllWorkFlowEntriesOrderedByCollateral(loan);
	}

	public void addWorkflowNote(WorkflowNote newWorkFlowNote) throws Exception {
		workFlowListHelper.addWorkflowNote(newWorkFlowNote);
		
	}
}
