package com.vintek.midtier.service;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.vintek.persistence.model.WorkflowEntry;

@Path("/wflow")
@RequestScoped
public class WorkflowRESTService {
	
   @Inject
   private EntityManager em;

   @GET
   @Produces("text/xml")
   public List<WorkflowEntry> listAllWorkflowEntries() {
      @SuppressWarnings("unchecked")
      //final List<WorkflowEntry> results = em.createQuery("select w from WorkflowEntry w order by w.id").getResultList();
      final List<WorkflowEntry> results = em.createNamedQuery(WorkflowEntry.ALL).getResultList();
      return results;
   }

   @GET
   @Path("/{id:[0-9][0-9]*}")
   @Produces("text/xml")
   public WorkflowEntry lookupWorkflowById(@PathParam("id") long id) {
      return em.find(WorkflowEntry.class, id);
   }
}
