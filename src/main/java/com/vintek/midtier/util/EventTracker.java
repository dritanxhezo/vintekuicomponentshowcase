package com.vintek.midtier.util;

import java.io.Serializable;
import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.event.Reception;
import javax.inject.Inject;


import com.vintek.persistence.model.WorkflowEntry;

@ApplicationScoped
public class EventTracker implements Serializable {
	private static final long serialVersionUID = 1942796227708746717L;
	
	@Inject
	private Logger log;

	public void onWorkFlowListChanged(@Observes(notifyObserver = Reception.IF_EXISTS) final WorkflowEntry workFlowEntry) {
		log.info("addWorkFlowEntryEvent was fired for: " + workFlowEntry);
	}

}
