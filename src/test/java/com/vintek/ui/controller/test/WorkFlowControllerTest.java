package com.vintek.ui.controller.test;

import static org.junit.Assert.assertNotNull;

import java.util.logging.Logger;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.vintek.persistence.model.WorkflowEntry;
import com.vintek.persistence.util.Resources;
import com.vintek.ui.controller.WorkFlowController;

@RunWith(Arquillian.class)
public class WorkFlowControllerTest {
   @Deployment
   public static Archive<?> createTestArchive() {
      return ShrinkWrap.create(WebArchive.class, "test.war")
            .addPackages(true, "com.vintek")
            .addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
            .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
            // Deploy our test datasource
            .addAsWebInfResource("test-ds.xml", "test-ds.xml");
   }

   @Inject
   WorkFlowController wfController;

   @Inject
   Logger log;

   @Test
   public void testRegister() throws Exception {
	  WorkflowEntry wfEntry = wfController.getNewWorkFlowEntry();
	  wfEntry.setCollateral("abc");
	  wfEntry.setStatus(123);
	  wfEntry.setTitle("title");
      wfController.addWorkFlow();
      assertNotNull(wfEntry.getId());
      log.info(wfEntry.getCollateral() + " was persisted with id " + wfEntry.getId());
   }
   
}
